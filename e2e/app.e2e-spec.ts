import { Report2Page } from './app.po';

describe('report2 App', () => {
  let page: Report2Page;

  beforeEach(() => {
    page = new Report2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
