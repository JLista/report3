import { Injectable, EventEmitter } from '@angular/core';

import { Observable, Subscription } from 'rxjs';

import { Report } from '../report';

import { RestApiService } from './rest-api.service';

@Injectable()
export class ReportsService {

  /* CONSTANTS */
  
  private prefixIndicator = "Team Survey";  //surveys which will be shown in this list need to start with Team Survey:

  /* VARIABLES */

  private reportDataPulled = false; //true if we are done getting data from SG

  private reports: Report[] = []; //array containing all surveys pulled from SG

  private responses: any[] = []; //array containing a list of user responses to surveys

  private relevantSurveys: any[] = []; //array containing all surveys that start with the Team Survey prefix


  /* DECLARE EVENT EMITTERS */
  
  reportsChanged = new EventEmitter<Report[]>();

  gotReport = new EventEmitter<Report>();

  gotReportList = new EventEmitter<any>();  

  gotSurveyAnswers = new EventEmitter<any>();


  /* INJECT HTTP SERVICE */
  
  constructor(private apiService: RestApiService) { }

   /*
   *Name: getResponses
   *Description: Sends array of responses to the subscription in the report detail component
   *input: the index of the report 
   */

  public getResponses(id: number){


    if (this.responses[id]==null){  //if we don't have it already
    
    //pull from API

    let apiSubscription: Subscription = this.apiService.fetchServlet("response",id.toString()).subscribe((responses)=>{

      this.responses[id] = responses

      this.gotSurveyAnswers.emit(responses); //emit the responses

      apiSubscription.unsubscribe();
    });

    }
    else{  //otherwise, just emit the responses

      this.gotSurveyAnswers.emit(this.responses[id]);
    }
  }


  /*
   *Name: getReport
   *Description: emits a specific report
   */

  public getReport(index: number) {

    let reportsChangedSubscription: Subscription = this.reportsChanged.subscribe((data: any) => {

        this.reports = data;
        this.reportDataPulled = true;
        this.emitThisReport(index);
        reportsChangedSubscription.unsubscribe();
      });

    if (this.reportDataPulled == true){ //if we already have this report

      this.emitThisReport(index); //emit it

    }
    else{


      this.pullSurveyList(index); //call function to get the list of surveys from API
      
    }
  }

  /*
   *Name: getReports
   *Description: emits all of the reports
   */

  public getReports(){

    if (this.reportDataPulled == false){ //if we don't have the reports

      this.pullSurveyList(-1); //call function to get them from API
    }
    else{ //otherwise, just emit them

      this.emitReports();
    }
  }

  /*
   *Name: extractSurveyTitle
   *Description: Removes the "Team Survey" prefix from the title of a survey
   *@input: an array of strings representing the title of a survey
   *@output: a string representing the title without the prefix
   */

  private extractSurveyTitle(title: string[]){

    let thisTitle="";

    for (var j = 1; j < title.length; j++){

      /*the string has previously been split by : to extract the prefix.
        in the case where the survey title has multiple colons, any other colons 
        which were removed in the split can be added back in.
        */

      if (j > 1){ 

        thisTitle = thisTitle.concat(":");
      }
      thisTitle = thisTitle.concat(title[j]);
    }
    return thisTitle;
  }

  /*Name: pullSurveyList
   *Description: gets a list of surveys from the API
   */

  private pullSurveyList(target: number){

    /*reportDataPulled is a flag used to indicate if we have already gotten the data.
     *the eventEmitters will sometimes be triggered multiple times, and we use this flag
     *to prevent that from happening.
     */

    this.reportDataPulled = false;

    //start out with an empty array of reports

    this.reports = [];

    //call the API list of surveys

    this.apiService.fetchServlet("survey","").subscribe((data)=>{

      this.gotReportList.emit(data); 

    });

    /*due to Angular2's async, it may attempt to start extracting the relevant surveys before
     *the api call is finished. The event emitter is to make sure the next block of code
     *is not triggered until the api call is finished. */

    let gotReportListSubscription: Subscription = this.gotReportList.subscribe((data) => {

      /*Do not try to extract the relevant surveys unless we have not done so already.
        reportDataPulled flag will be set to true once it has been done */
      
      if (!this.reportDataPulled){

        //get a list of only the surveys we want on the main list

        this.relevantSurveys = this.extractRelevantSurveys(data);

        //call function to extract that data and form our array

        this.collectSurveyData(this.relevantSurveys.length - 1, target);

        this.reportDataPulled = true;
      }
    });
  }

  /*
   *Name: collectSurveyData
   *Description: goes through list of relevant surveys (json objects returned from API), takes their data,
    uses it to create an array of objects of type Report which can be passed into the report-list.
   *@input: n (counter for recursion)
   */

  private collectSurveyData(n: number, target: number){

    if (n == -1 ){  //base case. Once we are done, emit the reports to the report-list component.

      this.emitReports();
    }
    else{

        //declare variables which will be filled with data from the survey

        let thisSurvey = this.relevantSurveys[n]; //get a survey from the list

        let thisId = thisSurvey['id'];
        
        let thisDate = thisSurvey['created_on'].split(/\s+/)[0];

        let thisNumResponses = 0;

        if (thisSurvey['statistics'] != null){

          let thisNumResponses = thisSurvey['statistics']['Complete'];

      

          //add the number of partial responses to the number of complete responses

          if (thisSurvey['statistics']['Partial'] > 0){

            thisNumResponses += thisSurvey['statistics']['Partial'];
          }

        }

        //extract the prefix from the survey title

        let prefix = thisSurvey['title'].split(':');

        let thisTitle = this.extractSurveyTitle(prefix);

        //fetch list of questions from the survey, which will contain the hidden variable info

        this.apiService.fetchServlet("question",thisId).subscribe((questions)=>{

            //the survey has four hidden variables containing information we want on our list

            let hiddenVars: string[] = [];

            let i = questions.length-1;

            while (hiddenVars.length < 4){

              if (questions[i]["_subtype"] == "hidden"){

                hiddenVars.push(questions[i]['properties']['defaulttext']['English']);
              }
              i--;
            }

          //once all variables have been collected from the API, use them to create a Report object
          //push it onto our reports array

          this.reports.push(new Report(thisTitle,                   //survey name
                                       hiddenVars[3],               //type
                                       hiddenVars[2],               //team name
                                       hiddenVars[1],               //client
                                       hiddenVars[0],               //team status
                                       thisDate,                    //survey date
                                       thisSurvey['links']['edit'], //SG URL
                                       (this.reports.length + 1),   //index
                                       thisNumResponses,            //# of responses
                                       thisId,                      //ID on SurveyGizmo
                                       questions,                   //json object containing questions
                                       false                        //selected=false (checkbox not checked)
                                       ));


          this.emitReports(); //emit the current report list. It will be displayed until the table until we get the next report.

          this.collectSurveyData(n-1, target); //call function recursively to get data for the next survey

          /*recursion is used here instead of a loop because we need to trigger the next interation from 
            inside a subscription block to avoid problems with async
           */
        });
    }
  }

  /*
   *Name: extractRelevantSurveys
   *Description: takes a list of surveys (json objects returned from API) and compiles a list containing
                 only the surveys which we want on our list (must start with "Team Survey:" prefix)
   *input(): Array of json objects
   *output: Array of json objects containing only the ones we want
   */

  private extractRelevantSurveys(data: any[]){

    let relevantSurveys: any[] = [];

    for (var i = 0; i < data.length; i++){

        let thisSurvey = data[i];

        let prefix = thisSurvey['title'].split(':');

        //check if the survey has our specified prefix for surveys we want to display
        
        if (prefix[0] == this.prefixIndicator){

          relevantSurveys.push( thisSurvey );
        }
      }
      return relevantSurveys;
    }

    /*
   *Name: emitReports
   *Description: Sends array of reports to the report-list component using an event emitter
   */

  private emitReports(){

    this.reportsChanged.emit(this.reports);

  }
  
 /*
  *Name: emitThisReport
  *Description: emits a specified report to the subscription in the report detail component
  *@input: the index of the requested report
  */


  private emitThisReport(index: number){

    for (var i = 0; i < this.reports.length; i++){

      if (this.reports[i].index == index){

        this.gotReport.emit(this.reports[i]);
        return;

      }
    }
  }
}
   
   