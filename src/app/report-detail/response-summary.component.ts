/**
 * Name: Response summary
 * 
 * Description: Shows a data for a report that is not specific to a respondent or question. This component
 *              appears on the display section of Report Detail when "Response Summary" is selected on the nav.
 */


import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { Subscription } from 'rxjs/Rx';

import { MenuModule, MenuItem } from 'primeng/primeng';

import { ReportsService } from '../services/reports.service';

import { Report } from '../report';

@Component({
  selector: 'sr-response-summary',

  templateUrl: 'response-summary.component.html',

  styleUrls: ['../stylesheets/content-box.css',
              '../stylesheets/global.css']

})
export class ResponseSummaryComponent {

  private repIndex: number;
  private selectedReport: Report;

  private reportSubscription: Subscription;

  public constructor(private reportsService: ReportsService,
                     private routes: ActivatedRoute) {}

  ngOnInit(){

      this.repIndex = +this.routes.snapshot['_urlSegment']['segments'][1]['path'];

      this.reportSubscription = this.reportsService.gotReport.subscribe(
      (rep: Report) => { 

        this.selectedReport = rep;
 
      });
      
      this.reportsService.getReport(this.repIndex);

  }
  ngOnDestroy() {

    this.reportSubscription.unsubscribe();
  }
}
