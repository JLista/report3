/**
 * Name: Respondent detail
 * 
 * Description: This component shows detail for some respondent, specified by an ID. That respondent's details
 *              are shown in the display section of the Report Detail component when the user clicks the name of 
 *              a respondent on the nav.
 */

import { Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { Subscription } from "rxjs/Rx";

import { ReportsService } from "../services/reports.service";
import { Report} from "../report";

@Component({
  selector: 'sr-respondentdetail',

  templateUrl: './respondent-detail.component.html',

  styleUrls: ['../stylesheets/content-box.css',
              '../stylesheets/global.css']
})
export class RespondentDetailComponent implements OnInit, OnDestroy {
  
  private reportIndex: number;

  private userIndex: number;

  private selectedReport: Report;
 
  private name: string;
  
  private reportSubscription: Subscription;

  private answerSubscription: Subscription;

  private routeSubscription: Subscription;

  constructor(private routes: ActivatedRoute, 
              private reportsService: ReportsService) {}


  ngOnInit() {

     //get the index of the report from the route snapshot

     this.reportIndex = +this.routes.snapshot['_urlSegment']['segments'][1]['path'];

     //get the index of the respondent from the route paramater subscription

     this.routeSubscription = this.routes.params.subscribe(
        (params: any) => {this.userIndex = params['id'];

        this.answerSubscription = this.reportsService.gotSurveyAnswers.subscribe( answers=> {

        //extract respondent name

        this.name  = (answers[this.userIndex]['[url("eeid")]']);

        this.answerSubscription.unsubscribe();

      });
    
      //get the report object for this report

      this.reportSubscription = this.reportsService.gotReport.subscribe(
      (rep: Report) => { 
        
        this.selectedReport = rep;

        this.reportsService.getResponses(this.selectedReport.sgId);
 
      });
      
      this.reportsService.getReport(this.reportIndex);
     
   });
      
  }
    
  ngOnDestroy() {

    this.reportSubscription.unsubscribe();
    this.answerSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
  }
}
