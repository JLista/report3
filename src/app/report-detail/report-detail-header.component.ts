/**
 * Name: Report Detail header
 * 
 * Description: This appears at the top of the Report Detail component. It displays the name of the report.
 */

import { Component, Input} from '@angular/core';
import { Report } from "../report";

@Component({
  selector: 'sr-report-detail-header',

  templateUrl: './report-detail-header.component.html',

  styleUrls: ['../stylesheets/report-detail-header.component.css',
              '../stylesheets/global.css']
})
export class ReportDetailHeaderComponent  {
  
  @Input() report: Report;  

}
