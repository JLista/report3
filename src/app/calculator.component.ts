import { Component, OnInit } from '@angular/core';
import { CalculatorService } from './services/calculator.service';

@Component({
  selector: 'sr-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./stylesheets/calculator.component.css',
              './stylesheets/side-nav.css' ]
})
export class CalculatorComponent implements OnInit {

  private operations = "";

  private firstEntered = false;
  private firstPoint = false;
  private opEntered = false;
  private secondEntered = false;
  private secondPoint = false;

  constructor(private calculatorService: CalculatorService) { }

  ngOnInit() {
  }

  private append(input: string){

    console.log("first " + this.firstEntered);
    console.log("pt " + this.firstPoint);
    console.log("op " + this.opEntered);
    console.log("second " + this.secondEntered);
    console.log("pt " + this.secondPoint);

    if (!this.firstEntered){

      if (/[0-9]/.test(input)){

          this.operations = this.operations.concat(input);
          this.firstEntered = true;
      }
    }
    else if (!this.opEntered){

        if (this.firstPoint && input == "."){
          return;
        }

        this.operations = this.operations.concat(input);

        if (input == "."){

          this.firstPoint = true;
        }

        else if (/[+*-/]/.test(input)){

          console.log("triggered op entered");

          this.opEntered = true;

        }
    }
    else if (!this.secondEntered){

      if (/[0-9]/.test(input)){
        this.operations = this.operations.concat(input);
        this.secondEntered = true;
      }
    }

    else{

        if (/[0-9]/.test(input)){

          this.operations = this.operations.concat(input);
          this.firstEntered = true;
        }

        else if (input == "." && !this.secondPoint){

          this.operations = this.operations.concat(input);
          console.log("second pt triggered");
          this.secondPoint = true;
        }
    }

  }

  private backspace(){

    this.operations = this.operations.substring(0,this.operations.length-1);
  }

  private clear(){

    this.operations = "";

    this.firstEntered = false;
    this.opEntered = false;
    this.secondEntered = false;
  }

  private closeCalc(){

    this.calculatorService.toggleCalc();
  }

  private evaluate(){


    var i = 0;
    var tokens: any[] = [];


  }


}
