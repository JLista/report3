import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { ReportsService } from './services/reports.service';
import { TableService } from './services/table.service';

import { Report } from './report'

import { ReportSymbolComponent } from './report-symbol.component';

import { MenuModule,MenuItem } from 'primeng/primeng';
import { LazyLoadEvent } from 'primeng/primeng';

@Component({
  selector: 'sr-report-list',

  templateUrl: 'report-list.component.html',
  
  styleUrls: ['./stylesheets/global.css',
              './stylesheets/data-table.css'],

})
export class ReportListComponent {


    private reports: any[];  //the set of all reports pulled from the API. Will be built asynchronously
    private reportsShown: any[] = []; //the set of reports which the table is currently displaying (changes on pagination or sorting)

    private searchResult: any[] = []; //the set of reports which match our search criteria (if search is used)

    private totalRecords = 0; //number of reports that we have in total
    private numRecords = 0; //number of reports we are currently showing (will be less than totalRecords if searched)
    private showingFirst: number; //the index of the first report on the current page of the table (changes on pagination)
    private showingLast: number; //the index of the last report on the current page of the table (changes on pagination)

    private reportsChangedSubscription: Subscription; 
    private screenResizedSubscription: Subscription; 
    private searchSubscription: Subscription; 

    private rowHeight = 53;

    private searchValue: string;

    private checkboxValue = false;

    private columnsHidden = false;

    private numRowsDefault = 5;
    private numRows;
    
    private spacerHeight = this.numRowsDefault * this.rowHeight;

    private tableHeight = 140 + this.spacerHeight;

    private tableHeightString = this.tableHeight+"px";   //dynamically calculated height of table, based on how many elements we want to display

    //this ensures the height of the page doesn't change if we move to a page with fewer than the maximum numbre of entries per page

    private apiLimit = "No Reports Loaded."; //constants for use in noRecords
    private noResults = "No results match your search."

    private noRecords = this.apiLimit; //message displayed when no entries can be loaded

    private sortType: string;
    private sortOrder: number;

    private bulkActionMenu = [
        {label: 'Select All', command: (event) => {

            if (this.reports){

                for (var i = 0; i < this.reports.length; i++){

                    this.reports[i].selected = true;
                }

            }
        }},
        {label: 'Deselect All', command: (event) => {

            if (this.reports){

                for (var i = 0; i < this.reports.length; i++){

                    this.reports[i].selected = false;
                }

            }
        }}
    ];

    public constructor(private reportsService: ReportsService, private tableService: TableService){}

    ngOnInit(){

        this.showingFirst = -1;

        if (window.innerWidth < 560){  //start in single view mode if screen is small

            this.columnsHidden = true;
            this.numRows = 1;
        }
        else{ //otherwise start in desktop mode

            this.numRows = this.numRowsDefault;
        }

        //if screen is resized, swtich view mode

        this.screenResizedSubscription = this.tableService.resizeScreenEmitter.subscribe((event) =>{

            var width = event.target.innerWidth; //get screen width

            var allShowing: any[]; 

            //determine which set of entries to display: either display all of them, or display the ones which 
            //match the search query if there is a search query

            if (this.searchResult[0] == null){

                allShowing = this.reports;
            }
            else{

                allShowing = this.searchResult;
            }

            if (width < 560 && !this.columnsHidden){  //if screen was previously larger than 700 and is resized to smaller than 700

                //switch to single view mode

                this.columnsHidden = true; 
                this.numRows = 1;

                if (this.reports){ //if any items are loaded

                    //display one of them

                    this.reportsShown = allShowing.slice(this.showingFirst, this.showingFirst+1); 

                }
            }
            else if (width > 560 && this.columnsHidden){ //if the screen is larger

                //switch to desktop mode

                this.columnsHidden = false;
                this.numRows = this.numRowsDefault;

                //set paging display

                this.showingFirst = 0;   
                this.showingLast = this.showingFirst + (allShowing.length < this.numRows ? allShowing.length : this.numRows);

                if (this.reports){

                    //show appropriate number of entries in table

                    this.reportsShown = allShowing.slice(this.showingFirst, this.showingLast); //switch to showing only one report

                }

            }

        });

        //this subscription will receive the reports list when the service creates it

        this.reportsChangedSubscription = this.reportsService.reportsChanged.subscribe((rep: Report[]) => {

            this.showingFirst = 0;

            this.reports = rep;

            if (this.columnsHidden ){

                //if we are in mobile mode, display only the first result

                this.reportsShown[0] = rep[0];

            }
            else if (rep.length <= this.numRowsDefault){

                //if we are in desktop mode and we aren't displaying the maximum number of entries per page, 
                //add another entry to the page

                this.reportsShown[this.totalRecords] = rep [this.totalRecords];
                this.showingLast++;

            }

            //update the number of stored records and the number of displayed records

            this.totalRecords = rep.length;
            this.numRecords = rep.length;

        });

        //if user enters a search query, filter the table results
    
        this.searchSubscription = this.tableService.searchedReports.subscribe((event)=>{

            if(this.reports){

                this.searchResult = [];

                for (var i = 0; i < this.totalRecords; i++){ //go through each entry in the table


                    let searchString = this.reports[i].reportName.concat(
                                    this.reports[i].type).concat(
                                    this.reports[i].teamName).concat(
                                    this.reports[i].client);

                    if (searchString.toUpperCase().includes(event.toUpperCase())){

                        this.searchResult.push(this.reports[i]); //if a report matches criteria, add it to the list
                    }
                }
                if(this.searchResult[0] == null){ //if no results are found

                    this.noRecords = this.noResults; //set message 
                    this.showingFirst = -1;  //this will cause the paging display to say "0 - 0 of 0"
                    this.showingLast = 0;
                    this.reportsShown = []; //display nothing
    
                }
                else if (this.columnsHidden){ //if results are found and we are in mobile view

                    this.reportsShown = [this.searchResult[0]]; //display a single record
                    this.showingFirst = 0;
                    this.noRecords = this.apiLimit; //switch back to default error message

                }
                else{ //if results are found and we are in desktop view

                    this.reportsShown = this.searchResult.slice(0,this.numRows); //display first page of search results
                    this.noRecords = this.apiLimit;
                    this.showingFirst = 0;
                    this.showingLast = this.showingFirst + (this.searchResult.length < this.numRows ? this.searchResult.length : this.numRows)*1;
                }
                this.numRecords = this.searchResult.length;
            }

        });

        this.reportsService.getReports(); //call function to create reports list
    }

    //for use in single view mode. When the right arrow is clicked, moves to next report.    

    private moveRight(){

        if (this.reports){

            var allShowing: any[];

            //decide if we will be paging through the whole list or the list of search results

            if (this.searchResult[0] == null){

                allShowing = this.reports;
            }
            else{

                allShowing = this.searchResult;
            }

            this.showingFirst = (this.showingFirst < allShowing.length-1 ? this.showingFirst+1 : 0); 
            //if we are on the first report, switch to the last, otherwise go to the next entry

            this.showingLast = this.showingFirst+1;
            
            this.reportsShown = allShowing.slice(this.showingFirst, this.showingLast); //update table view
            

        }

    }

    //for use in single view mode. When the left arrow is clicked, moves to next report.    

    private moveLeft(){

        if (this.reports){

            var allShowing: any[];

            if (this.searchResult[0] == null){
                allShowing = this.reports;
            }
            else{
                allShowing = this.searchResult;
            }

            this.showingFirst = (this.showingFirst > 0 ? this.showingFirst-1 : allShowing.length-1); 
            //if we are on the first report, switch to the last

            this.showingLast = this.showingFirst+1;
            
            this.reportsShown = allShowing.slice(this.showingFirst, this.showingLast); //update table view

            
        }

    }

    /*creates a context menu for each item on the table, brought up when user clicks the ellipses*/

    private getMenu(link: string){  //takes in the SurveyGizmo URL

        let items = [
                    {label: 'SurveyGizmo Results', url: link, target: '_blank'}
        ];

        return items;
    }

    //compare: the field of the report by which we want to sort
    //order: 1 is forward, -1 is backward. Do not input any other number to this function.

    private getSortReportFunction (compare: string, order: number){

        //create a function which compares two Reports based on the value of the field specified by "compare"
        //this function will be used by Javascript sort

        return function(a,b) {

            if (a[compare] < b[compare]){

                return -1*order;
            }
            if (a[compare] > b[compare]){

                return 1*order;
            }

            return 0;
        }      

    }

    /*Updates the view on the table when user performs sort, pagination, or additional reports are retrieved from the API
    *LazyLoadEvent contains information about the event which is needed to update the view
    */

    loadReportsLazy(event: any) {

        //event.first is the index of the first report to show in the table view

        if (event.first == null){  //in the case that event was sent from the num.rows select

            let rowCount = event;

            event = {

                rows: rowCount,
                first: this.showingFirst + 0,
                sortField: this.sortType,
                sortOrder: this.sortOrder
            };
            

        }

        this.showingFirst = event.first; 

        //the last element to show in the view is the first element plus the number of elements shown per page (event.rows)

        //unless this sum is larger than the total number of reports, in which case the last element to display is the last report

        this.showingLast = ( event.first + event.rows > this.numRecords ? this.numRecords: (event.first + event.rows) )*1;

        this.numRows = event.rows;

        //if reports are loaded, perform actions to update the table view

        if (this.reports){

            var allShowing: any[];

            if (this.searchResult[0] == null){

                allShowing = this.reports;
            }
            else{

                allShowing = this.searchResult;
            }

            //when the user clicks a sort button, the field "sortField" tells us which column we need to sort by
            //if it isn't specified, default to sorting by report name

            let choice = ( event.sortField == null ? 'reportName': event.sortField ); 

            this.sortType = event.sortField;
            this.sortOrder = event.sortOrder;

            //sort the list of reports based on the specified sort order and column

            allShowing = allShowing.sort(this.getSortReportFunction(choice, event.sortOrder));

            //pull a list of reports we are going to show on this view from the total list of reports

            this.reportsShown = allShowing.slice(this.showingFirst, this.showingLast);

        }
        
    }

    //when the select all checkbox is clicked, set all reports to selected

    selectVisible(event: Event){

        for (var i = 0; i < this.reportsShown.length; i++){

            this.reports[this.reportsShown[i].index - 1].selected = event;
        }
    }

    ngOnDestroy(){

        this.reportsChangedSubscription.unsubscribe();
        this.screenResizedSubscription.unsubscribe();
        this.searchSubscription.unsubscribe();

    }
}



