import { Component, OnInit, Input } from '@angular/core';
import { ColorCodedCell} from "./colorCodedCell.component";

@Component({
  selector: 'sr-heatmap',
 
  template: `

            <table class ="dataRow"><tr>
                <td class = "nameColumn"> 
                    {{userName}}
                </td>
                <td *ngFor = "let rating of ratings; let i = index" class = "dataColumn">

                <div *ngIf = "ratings[0]">
                    <color-cell [value] = "ratings[i]"></color-cell>
                </div>
                </td>
                <td class = "dataColumn">
                    
                    <color-cell [value] = "overallRating"></color-cell>

                </td>
            </tr></table>
            
  `,
 
  styleUrls: ['../stylesheets/detailTable.css']
})

export class HeatMapComponent implements OnInit {

    @Input() userName: string;
    @Input() ratings: number[] = [];
    @Input() overallRating: number;
  
    constructor() {}

    ngOnInit(){

    }
    
}
