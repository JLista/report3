import { Component } from '@angular/core';
import { TableService } from './services/table.service';
import { CalculatorService } from './services/calculator.service';

@Component({
  selector: 'sr-side-nav',
  templateUrl: 'side-nav.component.html',
  styleUrls: ['./stylesheets/side-nav.css',
              './stylesheets/global.css',
              './stylesheets/content-box.css']
})
export class SideNavComponent{

  private expandedNav: boolean[] =[true,true,true]; //each bool represents one of the menu items
                                                                    //when they are true, that menu is expanded

  constructor(private tableService: TableService,
              private calculatorService: CalculatorService) { }

  private toggleNav(index: number){ //when user clicks on a menu item, expand that menu item

    this.expandedNav[index] = this.expandedNav[index]? false:true;
  }
  
  private toggleCalc(){

    this.calculatorService.toggleCalc();
  }

  private closeNav(){

    this.tableService.toggleNav();
  }



}
