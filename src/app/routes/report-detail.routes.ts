import { Routes } from "@angular/router";

import { ReportDefaultComponent } from "../report-detail/report-default.component";
import { RespondentDetailComponent } from "../report-detail/respondent-detail.component";
import { ResponseSummaryComponent} from "../report-detail/response-summary.component"

export const REPORT_DETAIL_ROUTES: Routes = [

    { path: '', component: ReportDefaultComponent},
    { path: 'details', component: ReportDefaultComponent},
    { path: 'respondent', component: RespondentDetailComponent},
    { path: 'summary', component: ResponseSummaryComponent},
    { path: 'respondent/:id', component: RespondentDetailComponent}

];