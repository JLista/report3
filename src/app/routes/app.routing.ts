import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";

import { ReportListComponent } from "../report-list.component";
import { ReportDetailComponent } from "../report-detail/report-detail.component";
import { LoginComponent } from "../login.component";
import { ReportsComponent } from "../reports.component";

import { REPORT_ROUTES } from "../routes/report.routes";

const routes: Routes = [

    { path: '', component: LoginComponent, pathMatch: 'full' },
    { path: 'reports', component: ReportsComponent, children: REPORT_ROUTES},
    
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {useHash: false});
