import { Component, OnInit, OnDestroy } from '@angular/core';

import { MenuModule,MenuItem } from 'primeng/primeng';

import { TableService } from "./services/table.service";

import { Subscription } from 'rxjs/Rx';


@Component({
  selector: 'sr-header',

  templateUrl: 'header.component.html',

  styleUrls: ['./stylesheets/header.component.css',
              './stylesheets/global.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  private items: MenuItem[];
  private hamburger: MenuItem[];

  private screenResizedSubscription: Subscription;

  private searchValue: string;

  private searchWidth: string;

  private cursorType;

  private logoDisplay = true;

  public constructor(private tableService: TableService) {}

  ngOnInit(){

      //set the width of the search bar according to screen width


      if (window.innerWidth < 560){


          this.searchWidth = '30px';
          this.cursorType = "pointer";
          
      }
      else {

          this.searchWidth = '350px';
          this.cursorType = "text";
      }

      //if screen is resized, adjust search box width

      this.screenResizedSubscription = this.tableService.resizeScreenEmitter.subscribe((event) =>{

          var width = event.target.innerWidth;

          if (width < 560){

              this.searchWidth = '30px';
              this.cursorType = "pointer";
          }
          if (width >= 560){

              this.searchWidth = '350px';
              this.logoDisplay = true;
              this.cursorType = "text";
          }


        });

       this.items = [
                      {label: 'Log Out', icon: 'fa-sign-out', url: 'http://localhost:4200'}
                    ];

  }
  
  private openSearchBar(){
    if (window.innerWidth < 560){

      let searchWidthDimension = window.innerWidth - 106;
      let innerWidthString = searchWidthDimension + "px"

      //this.searchWidth = (this.searchWidth == '30px') ? innerWidthString:'30px';
      //this.logoDisplay = (this.logoDisplay)? false:true;
      this.searchWidth = innerWidthString;
      this.logoDisplay = false;
      this.cursorType = "text";
    }
  }
  private closeSearchBar(){
    if (window.innerWidth < 560){

      let searchWidthDimension = window.innerWidth - 106;
      let innerWidthString = searchWidthDimension + "px"

      //this.searchWidth = (this.searchWidth == '30px') ? innerWidthString:'30px';
      //this.logoDisplay = (this.logoDisplay)? false:true;
      this.searchWidth = '30px';
      this.logoDisplay = true;
      this.cursorType = "pointer";
    }
  }
  
  /*
   *name: valueChanged
   *description: called when user enters a search query. Calls function in the table service to perform the search.
   */

  private valueChanged(event: Event){

    this.tableService.search(event);
  }

  /*
   *name: toggleNav
   *description: called when user clicks to open or close the hamburger menu. Calls the function in the table service
   *to toggle the menu state.
   */

  private toggleNav(){

    this.tableService.toggleNav();
  }

  /*
   *name: resizeScreen
   *description: called when user resizes the screen. Calls the function in the table service to handle responsive changes.
   */

  resizeScreen(event: Event){

      this.tableService.resizeScreen(event);
  }

  ngOnDestroy(){

    this.screenResizedSubscription.unsubscribe();

  }
}
