import { NgModule, ApplicationRef, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ReportsService } from './services/reports.service';
import { RestApiService } from './services/rest-api.service'
import { TableService } from './services/table.service';
import { CalculatorService } from './services/calculator.service';

import { routing } from './routes/app.routing'

// App is our top level component
import { AppComponent } from './app.component';

//PrimeNG Components

import { PanelMenuModule } from 'primeng/primeng';
import { MenuModule,MenuItem } from 'primeng/primeng';
import { DataTableModule,SharedModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { LazyLoadEvent } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { PaginatorModule } from 'primeng/primeng';

//Material Components

import { MdSidenavModule } from '@angular/material';

// Core providers

import { ReportListComponent } from './report-list.component';
import { ReportsComponent } from './reports.component';
import { LoginComponent } from './login.component';
import { HeaderComponent } from "./header.component";

import { ReportDetailComponent } from "./report-detail/report-detail.component";
import { ReportDefaultComponent } from "./report-detail/report-default.component";
import { RespondentDetailComponent } from "./report-detail/respondent-detail.component";
import { ReportDetailHeaderComponent } from './report-detail/report-detail-header.component';
import { ResponseSummaryComponent } from './report-detail/response-summary.component'

import { ColorCodedCell } from "./displays/colorCodedCell.component";
import { HeatMapComponent } from "./displays/heatmap.component";
import { ReportSymbolComponent } from './report-symbol.component';
import { SideNavComponent } from './side-nav.component';
import { CalculatorComponent } from './calculator.component';



// Application wide providers
const APP_PROVIDERS = [
];


/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    ReportListComponent,
    HeaderComponent,
    ReportDetailComponent,
    ReportsComponent,
    LoginComponent,
    ReportDetailHeaderComponent,
    ReportDefaultComponent,
    RespondentDetailComponent,
    ColorCodedCell,
    HeatMapComponent,
    ResponseSummaryComponent,
    ReportSymbolComponent,
    SideNavComponent,
    CalculatorComponent
    
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    FormsModule,
    HttpModule,
    DataTableModule,
    BrowserAnimationsModule,
    SharedModule,
    MenuModule,
    InputTextModule,  
    PanelMenuModule,
    CheckboxModule,
    MdSidenavModule,
    PaginatorModule,
    routing
  ],
  exports: [
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    // ENV_PROVIDERS,
    APP_PROVIDERS, ReportsService, RestApiService, TableService, CalculatorService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}
}

