import { Component, OnDestroy, ViewChild, ElementRef } from '@angular/core';

import { MdSidenavModule,MdSidenav } from '@angular/material';

import { Subscription } from 'rxjs/Rx';

import { TableService } from './services/table.service';

import { SideNavComponent } from './side-nav.component';

@Component({
  selector: 'app-root',
  
  templateUrl: 'app.component.html',

  styleUrls: ['./stylesheets/app.component.css',
              './stylesheets/global.css']

})
export class AppComponent {

  private showDevBuild = false;


  public constructor(private tableService: TableService) {}



}